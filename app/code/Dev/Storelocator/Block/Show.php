<?php
namespace Dev\Storelocator\Block;


use Dev\Storelocator\Api\StoreRepositoryInterface;
use Dev\Storelocator\Model\ResourceModel\Store\CollectionFactory;
use Dev\Storelocator\Model\Store;
use Dev\Storelocator\Model\StoreFactory;
use Magento\Framework\View\Element\Template;


class Show extends Template
{
    private StoreFactory $storeFactory;
    private StoreRepositoryInterface $storeRepository;
    private CollectionFactory $collectionFactory;

    public function __construct(
        StoreFactory $storeFactory,
        StoreRepositoryInterface $storeRepository,
        CollectionFactory $collectionFactory,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->storeFactory = $storeFactory;
        $this->storeRepository = $storeRepository;
        $this->collectionFactory = $collectionFactory;
    }


//    protected function _prepareLayout()
//    {
//        $this->pageConfig->getTitle()->set('My First Blog in first.loc');
//        return parent::_prepareLayout();
//    }

    public function getStoreCollection()
    {
       $res= $this->storeFactory->create();
//       $res->
        $cres=$this->collectionFactory->create()->getItems();

//        var_dump($res);die();

        return $cres;
    }

    public function getStoreById($storeId)
    {
        return $this->storeRepository->getById($storeId);
    }

    public function getCountry(){

        $sto =$this->storeFactory->create();


        return $sto;

    }




}
