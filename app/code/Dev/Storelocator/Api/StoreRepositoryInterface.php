<?php

namespace Dev\Storelocator\Api;

use Dev\Storelocator\Model\Store;

interface StoreRepositoryInterface
{
    public function getById($id);

    public function save(Store $store);

    public function delete(Store $store);

    public function deleteById($id);
}
