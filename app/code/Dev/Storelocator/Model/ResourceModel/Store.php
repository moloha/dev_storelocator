<?php

namespace Dev\Storelocator\Model\ResourceModel;

use Dev\Storelocator\Api\StoreInterface;

class Store extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init(StoreInterface::TABLE_NAME, StoreInterface::ENTITY_ID);
    }
}
