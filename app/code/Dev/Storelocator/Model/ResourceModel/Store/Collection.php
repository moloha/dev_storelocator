<?php


namespace Dev\Storelocator\Model\ResourceModel\Store;

use Dev\Storelocator\Model\Store;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Store::class,
            \Dev\Storelocator\Model\ResourceModel\Store::class
        );
    }





}
